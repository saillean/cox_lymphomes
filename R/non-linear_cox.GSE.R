###############################################################################

# 3.  Cox non-linear function

#Author : Angélique Saillet 
#mail : angelique.saillet@etu.univ-grenoble-alpes.fr
############################################################################## 



#function that apply a Cox non linear-gene model 
#return different output as the model, R2, AIC & different plots (ROC, martingale,Cox Snell & Score residues,Concordance with IC)
library(survival)
library(survivalROC)
library(CoxR2)
library(splines)
library(rms)

nonlinearCox.analyze<-function(gene.code,graph=TRUE,exit.time=max(clinical.db$follow),n.roc.crb=3,degree=3,rna.exp="log2base",centered=F){
  # the parameter gene.code defines the specific gene on which the model is applied
  
  #the mutations parameter allows to decide on which data set the model is applied according to the mutation status of the observations i.e. 
  ## if mutations = "ALL" then the model is applied on all observations independently of their mutation status (by default)
  ## if mutations = TRUE then the model is applied only on the observations for which the gene is mutated. 
  ## if mutations = FALSE then the model is applied only on the observations for which the gene is not mutated. 
  
  #The graph parameter allows to decide if we want to show the graphics results or not, TRUE by default. 
  #the argument exit.time allows to set an exit date of the study at a certain time. The data will be modified such that observations 
  #with survival times greater than exit.time will be replaced by this value and the observations will be considered as censored. 
  #It is set, by default, as the maximum of survival time found in the data.  
  
  #n.roc.crb argument is the number of ROC curves wished to be ploted. We put 3 by default. 
  
  #degree argument is the degree of the splines fitted between knots. 
  
  #rna.expr is the transformation applied on the rna expression of gene. It could be "log2base", "crudeRPM", "sqrt(crudeRPM)".  
  
  #---------------------------------------------------- PRE-TREATMENT-------------------------------------------------------
  #Observations selection according to the mutations parameter
  rna<-as.numeric(rna.db[,which(colnames(rna.db)==gene.code)])
  if(centered){
    rna<-rna-mean(rna)
  }
  clinical<-clinical.db
  
  #Setting the exit.time
  #getting the indexes of them who have T > exit.time 
  idc.ExT<-which(clinical$follow>exit.time)
  #replace the T by the exit.time and change the censor state as 0  
  clinical$follow[idc.ExT]<- exit.time
  clinical$censor[idc.ExT]<-0
  
  #Apply the transformation on rna vector. 
  if(rna.exp=="crudeRPM"){
    rna<-exp(rna*log(2))#deloger
  }
  
  if(rna.exp=="sqrt(crudeRPM)"){
    rna<-sqrt(exp(rna*log(2)))#sqrt(deloger)
  }
  
  #print(paste0("Cox model apply on ",rna.exp," rna expression"))
  
  #-------------------------------------------------------------------------------------------------------------------------
  
  #model application
  survie<-Surv(as.numeric(clinical$follow),clinical$censor)
  cox.gene<-coxph(survie ~ pspline(rna,df=0,degree=degree))
  
  #the tools used to make the titles indicative 
  #title
    mutations.title=paste("all observations (n=",length(rna),")")
  
  ############################################################################################################################
  #                                                     NUMERIC TOOLS
  ############################################################################################################################
  
  #------------------------------------------------------- AIC ---------------------------------------------------------------
  AIC=AIC(cox.gene)
  #---------------------------------------------------------------------------------------------------------------------------
  
  #------------------------------------------------- Concordance + CI (95%) --------------------------------------------------
  conc<-concordance(cox.gene)
  concordance<-conc$concordance
  concordance.IC.lower<-conc$concordance-sqrt(conc$var)*1.96
  concordance.IC.upper<-conc$concordance+sqrt(conc$var)*1.96
  #---------------------------------------------------------------------------------------------------------------------------
  
  #---------------------------------------------- Log-Likelihood test p.value ------------------------------------------------
  LRatio.pvalue<-summary(cox.gene)$logtest[3]
  #---------------------------------------------------------------------------------------------------------------------------
  
  #------------------------------------------------- LogRank test p.value ----------------------------------------------------
  LRank.pvalue<-NA
  #---------------------------------------------------------------------------------------------------------------------------
  
  #------------------------------------------------- Wald test p.value ----------------------------------------------------
  Wald.pvalue<-1 - pchisq(cox.gene$wald.test, df = cox.gene$df)
  #---------------------------------------------------------------------------------------------------------------------------
  
  #-------------------------------------------- Kolmogorov-Smirnov test p.value -----------------------------------------------
  #potentiellement à afficher sur le graph mais pas stocker
  #P.value du test de Kolmogorov-Smirnov sur l'adéquation des résidus de Cox-Snell a une distribution exponentielle E(1)
  resid_coxsnell <- -(residuals(cox.gene,type="martingale") - clinical$censor)
  KS.pvalue<-ks.test(resid_coxsnell,"pexp",1,exact=T)$p.value
  #---------------------------------------------------------------------------------------------------------------------------
  
  
  ############################################################################################################################
  #                                                     GRAPHIC TOOLS (only if argument graph=T)
  ############################################################################################################################
  
  ################################# Récupération des noeuds dans un vecteur ##################################################
  
  t<-paste(cox.gene$printfun)
  
  #traitement de la chaine/fonction pour récupérer numériquement les noeuds
  
  idc.b<-0
  knots<-c()
  for(c in 1:nchar(t)){
    #cas de démarrage
    if(substring(t,c,c+9)=="cbase = c("){
      idc.b<-c+10
    }
    #cas de cut
    if((idc.b!=0)&(substring(t,c,c)==',')){
      knots<-c(knots,as.numeric(substring(t,idc.b,c-1)))
      idc.b=c+1
    }
    #cas d'arrêt 
    if(substring(t,c,c)==')'){
      knots<-c(knots,as.numeric(substring(t,idc.b,c-1)))
      break;
    }
  }
  
  
  if(graph==T){
    par(mfrow=c(2,2))
    #----------------------------------------------------- log(Risk) --------------------------------------------------------
    pred<-predict(cox.gene,type="risk",se=T)
    plot(rna,log(pred$fit),ylim=c(-1,1),ylab="log(risk)",cex.main=1,col='blue',main=paste0("log(Risk) function according to RNA level of ",gene.code,"\n * degree = ",degree,"*"),cex.main=1)
    # Calculate the confidence interval of the curve
    # Set the confidence level
    conf.level <- 0.95
    idc.srtd<-order(rna)
    # Compute the critical t-value for the given confidence level and degrees of freedom
    df <- nrow(clinical.db) - length(cox.gene$coefficients)
    tval <- qt(1 - (1 - conf.level)/2, df)
    
    # Compute the upper and lower limits of the confidence interval
    upper <- log(pred$fit + tval * pred$se.fit)
    lower <- log(pred$fit - tval * pred$se.fit)
    
    lines(rna[idc.srtd], upper[idc.srtd], lty = 2,col='blue')
    lines(rna[idc.srtd], lower[idc.srtd], lty = 2,col='blue')
    #---------------------------------------------------------------------------------------------------------------------------
    
    #---------------------------------------- Martingale residuals to RNA level  -----------------------------------------------
    plot(rna,residuals(cox.gene,type="martingale"),main=paste0("Martingale residuals according to RNA level of ",gene.code,"\n * ",mutations.title," , exit.time = ",exit.time," *"),cex.main=1,ylab="martingale residuals",xlim=c(min(min(knots),min(rna))-0.2,max(max(knots),max(rna))+0.2))
    # #we add the lines corresponding to the knots given to the non linear model 
    abline(v=knots,col='darkgrey')
    # Calculate the martingale residuals trend using loess
    loess_model <- loess(residuals(cox.gene,type="martingale") ~ rna)
    idc.srtd<-order(rna)
    pred<-predict(loess_model,newdata=rna,se=T)
    
    lines(rna[idc.srtd],pred$fit[idc.srtd], col = "red",type='l',lwd=2.5)
    
    # Calculate the confidence interval of the curve
    # Set the confidence level
    conf.level <- 0.95
    # Compute the critical t-value for the given confidence level and degrees of freedom
    df <- nrow(clinical) - length(cox.gene$coefficients)
    tval <- qt(1 - (1 - conf.level)/2, df)
    
    # Compute the upper and lower limits of the confidence interval
    upper <- pred$fit + tval * pred$se.fit
    lower <- pred$fit - tval * pred$se.fit
    
    lines(rna[idc.srtd], upper[idc.srtd], lty = 2,col='red')
    lines(rna[idc.srtd], lower[idc.srtd], lty = 2,col='red')
    
    #---------------------------------------------------------------------------------------------------------------------------
    
    #------------------------------------------------- Cox-Snell residuals -----------------------------------------------------
    fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, clinical$censor) ~ 1,
                          ties    = c("efron","breslow","exact")[1])
    df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
    plot(df_base_haz$time,df_base_haz$hazard,col='red',type='b',xlim=c(0,max(df_base_haz$time)+0.2),ylim=c(0,max(df_base_haz$hazard)+0.2),lwd=1.5,xlab="Cox-Snell residuals",ylab=" ",main=paste0("Cox-Snell residuals for  ",gene.code,"\n * ",mutations.title," , exit.time = ",exit.time," *"),cex.main=1)
    abline(a=0,b=1,lty=2)
    #---------------------------------------------------------------------------------------------------------------------------
    
    #------------------------------------------------- Schoenfeld residuals ---------------------------------------------------
    # sch.t<-cox.zph(cox.gene)
    # plot(sch.t,col='red',main=paste0("Schoenfeld residuals according to time for ",gene.code,"\n * ",mutations.title," , exit.time = ",exit.time," *, p.v = ",round(sch.t$table[1,3],3)),lwd=2,cex.main=1,ylab="Schoenfeld residuals")
    #---------------------------------------------------------------------------------------------------------------------------
    
    #---------------------------------------------------- ROC Curves -----------------------------------------------------------
    if(n.roc.crb>0){
    #Settings for t
    t<-seq(0,exit.time,length.out=n.roc.crb+2)
    t<-round(t[-c(1,length(t))],1)
    
    plot.ROC.curves.nonlin(clinical,rna,cox.gene,t,degree=degree)
    }
    #---------------------------------------------------------------------------------------------------------------------------
    
    
    #------------------------------------------- Score residuals to RNA level --------------------------------------------------
    # for ( i in 1:length(cox.gene$coefficients)){
    # plot(rna,residuals(cox.gene,type='score')[,i],main=paste0("Score residuals according to RNA level of ",gene.code,"\n * ",mutations.title," * (for the coef ",i," of the spline)"),cex.main=1,ylab="score residuals")
    # if(mutations=="ALL"){
    #   points(rna[idc.m],residuals(cox.gene,type="score")[idc.m,i],col='green')
    #   legend("bottomright",legend="mutated gene",col='green',pch=1)
    # }
    # # Calculate the martingale residuals trend using loess
    # loess_model <- loess(residuals(cox.gene,type="score")[,i] ~ rna)
    # idc.srtd<-order(rna)
    # pred<-predict(loess_model,newdata=rna,se=T)
    # 
    # lines(rna[idc.srtd],pred$fit[idc.srtd], col = "red",type='l',lwd=2.5)
    # 
    # # Calculate the confidence interval of the curve
    # # Set the confidence level
    # conf.level <- 0.95
    # # Compute the critical t-value for the given confidence level and degrees of freedom
    # df <- nrow(clinical) - length(cox.gene$coefficients)
    # tval <- qt(1 - (1 - conf.level)/2, df)
    # 
    # # Compute the upper and lower limits of the confidence interval
    # upper <- pred$fit + tval * pred$se.fit
    # lower <- pred$fit - tval * pred$se.fit
    # 
    # lines(rna[idc.srtd], upper[idc.srtd], lty = 2,col='red')
    # lines(rna[idc.srtd], lower[idc.srtd], lty = 2,col='red')
    # }
    #---------------------------------------------------------------------------------------------------------------------------
    
    #-------------------------------------------------- Calibration ------------------------------------------------------------
    # cox.gene2<-cph(survie ~ rna,x=TRUE, y=TRUE,surv=TRUE, time.inc =1, dxy = TRUE)
    # c1<-calibrate(cox.gene2,u=1)
    # plot(c1,xlab='Predicted 1 Month Survival',ylab='Fraction Surviving  1 Month',main='Calibration')   
    #---------------------------------------------------------------------------------------------------------------------------
  }
  results<-data.frame("AIC"=AIC,"concordance"=list('value'=concordance,'concordance.IC.lower'= concordance.IC.lower,'concordance.IC.upper'=concordance.IC.upper),'LRatio.pvalue'=LRatio.pvalue,'LRank.pvalue'=LRank.pvalue,'Wald.pvalue'=Wald.pvalue)
  rownames(results)<-"non linear"
  return(list('model'=cox.gene,"results"=results))
}


#------------------- functions needed to build ROC curves-----------------------------------------

#Fonction called in linearCox.analyze() to build ROC curve for each t chosen with CI construct by bootstraping and taking quantiles 0.025 and 0.975.
#it returns nb.roc.crb ROC curves of our model for different values of times.  
plot.ROC.curves.nonlin<-function(clinical,rna,cox.gene,t,n.boot=500,nb.FP.fixe=50,degree){
  # clinical,rna and cox.gene arguments contains the clinical db, rna db and cox.gene modele computed in the linearCox.analyze() function
  # t argument contains the times for each we want to build a ROC curve + CI (it couldn't contains t>=exit.time)
  # n.boot argument is the number of bootstrap samples computed for each curve (i.e. each t values)
  # nb.FP.fixe argument is the number of points we fixed to buil the CI for a curve. Bigger FP.fixe is, more precise the CI would be but more long the computation time would also be.  
  
  
  #We create ROC curve for each times (t) with the main.model 
  scr<-predict(cox.gene,type='lp')
  main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
  
  #We get AUCs for each time (t) + we compute the CI 95% with TCL
  AUC<-main.roc$AUC[2]
  se<-main.roc$inference$vect_sd_1[2]
  AUC.low<-AUC-se*qnorm(0.975)
  AUC.up<-AUC+se*qnorm(0.975)
  
  #We fixe some FP points
  FP.fixe<-seq(0,1,length.out=nb.FP.fixe)
  
  #we create functions for the intervals bounds 95%
  borne.inf<-function(x){
    return(quantile(x,0.025,na.rm=T))
  }
  borne.sup<-function(x){
    return(quantile(x,0.975,na.rm=T))
  }
  
  #We compute the ROC bootstrap procedure for each t 
  for(c.t in 1:length(t)){
    bootst.TP<-c()
    
    for(i in 1:n.boot){
      indices<-sample(nrow(clinical), replace = TRUE)
      rslt<-boot_roc.nl(clinical,rna,indices,t,c.t,FP.fixe,degree=degree)
      bootst.TP<-rbind(bootst.TP,rslt)
    }
    
    #We get the bounds for each fixed points
    IC.low<-apply(bootst.TP,2,borne.inf)
    IC.sup<-apply(bootst.TP,2,borne.sup)
    
    #We plot the ROC curve and the CI at 95% 
    plot(0.5,0.5,col='white',xlim=c(0,1),ylim=c(0,1),cex.main=1,main=paste0("Courbe ROC a l'instant t = ",t[c.t]," (annees) \n AUC = ",round(AUC[c.t],2)," [",round(AUC.low[c.t],2),",",round(AUC.up[c.t],2),"]"),xlab="FP",ylab="TP")
    plot(main.roc,time=t[c.t],add=TRUE)
    lines(FP.fixe,IC.low,col='red',lty=2)
    lines(FP.fixe,IC.sup,col='red',lty=2)
    
  }
} 


# Intermediate fonction used in plot.ROC.curves.lin() to compute the bootstrap procedure for each t value, this function generates generates a ROC curve (FP is fixed so it returns only the TP) for the t value asked. 
# it return a vector of TP points corresponding to the ROC curve construction (with FP fixed points) for the bootstrap samples of our data (clinical,rna) defined by indices. For it it computed a new linear cox model with the bootstrap sample. 
boot_roc.nl <- function(clinical,rna,indices,t,c.t,FP.fixe,degree) {
  # clinical,rna and cox.gene arguments contains the clinical db, rna db and cox.gene modele computed in the linearCox.analyze() function
  # indices is the vector of indexes that we used to define the bootstrap sample used here.
  # c.t is the indec of the t value treated in the t vector given in the call of plot.ROC.curves.lin().
  # FP.fixe argument is the fixed FP points we used to build the CI for a curve.  
  
  
  survie<-Surv(as.numeric(clinical$follow[indices]),clinical$censor[indices])
  cox.gene2<-coxph(survie ~ pspline(rna[indices],df=0,degree=degree))
  scr<-predict(cox.gene2,type='lp')
  roc <- timeROC(clinical$follow[indices], delta=clinical$censor[indices],marker=scr,times=t[c.t],cause=1)
  TP.rslt<-roc$TP[,2]
  FP.rslt<-roc$FP[,2]
  #On construit notre vecteur "resume" à partir des 27 points fixés choisis 
  TP.fin<-rep(NA,length(FP.fixe))
  TP.fin[1]<-0
  TP.fin[length(FP.fixe)]<-1
  for(j in 2:(length(FP.fixe)-1)){
    # On cherche la valeur la plus proche dans celle calculé par l'algorithme ROC 
    for(g in 1:(length(FP.rslt)-1)){
      if(is.na(FP.rslt[g])){
        break
      }
      if((FP.rslt[g]<FP.fixe[j])&(FP.rslt[g+1]>FP.fixe[j])){
        TP.fin[j]<-mean(TP.rslt[g],TP.rslt[g+1])
        break
      }
      if(FP.rslt[g+1]==FP.fixe[j]){
        TP.fin[j]<-TP.rslt[g+1]
        break
      }
      
    }
  }
  return(TP.fin)
}


#-------------------------------------------------------------------------------------------------





