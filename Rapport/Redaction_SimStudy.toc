\contentsline {section}{\numberline {1}Simulation de données de survie selon un modèle de Cox}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Données non censurées}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Données censurées à droite}{3}{subsection.1.2}%
\contentsline {section}{\numberline {2}Explication de la procédure}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Etape de pré-estimation réaliste en fonction du cadre de la simulation}{4}{subsection.2.1}%
\contentsline {subsubsection}{\numberline {2.1.1}selon un modèle de cox linéaire basé sur l'expression du gène MYC\nobreakspace {}}{4}{subsubsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.2}selon un modèle non linéaire basé sur l'expression du gène TP53\\ }{4}{subsubsection.2.1.2}%
\contentsline {subsection}{\numberline {2.2}Procédure générale}{5}{subsection.2.2}%
\contentsline {section}{\numberline {3}Résultats : Etude de simulation}{7}{section.3}%
\contentsline {subsection}{\numberline {3.1}Indicateurs numériques}{7}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Indicateurs graphiques}{9}{subsection.3.2}%
\contentsline {section}{\numberline {4}Annexes}{11}{section.4}%
