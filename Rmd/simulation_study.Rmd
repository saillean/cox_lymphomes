---
title: "Etude de simulation"
output: pdf_document
date: "2023-05-22"
header-includes:
    \usepackage{algorithm}
    \usepackage{algorithmic}
    \usepackage{bbold}
    \usepackage{amssymb}
---

```{r setup,echo=FALSE,message=FALSE,warning=FALSE}
source("../R/linear_cox.GSE.R")
source("../R/non-linear_cox.GSE.R")

load(file = "../R/data/clinical.db.duke")
load(file = "../R/data/rna.db.duke")
```

\tableofcontents
\newpage

# 1. Introduction 

# 2. Etude de simulation de données réalistes linéaire à partir de l'expression du gène MYC 

## 2.1 Explication de la procédure

### 2.1.1 Etape d'estimation pré-simulation réaliste 

Afin de pouvoir simuler des données selon un cox linéaire réaliste pour notre contexte. Nous allons préalablement estimé : 

- un modèle de cox linéaire à partir des données du gène MYC qui semble présenter un comportement linéaire relativement à la significativité de la p.value associée au modèle. Ce modèle nous permet de récupérer l'estimation d'un coefficient linéaire réaliste $\hat{\beta}$ et l'estimation de la fonction de risque cumulé $\hat{H}(t)$ selon Nelson-Aalen. 

- la loi de la censure $C_i$ à partir d'un Kaplan-Meier. Cela nous permet de récupérer l'estimation de la fonction $\hat{G}(t)$. 

Ces estimations ainsi que la moyenne $\mu_{MYC}$ et l'écart-type $\sigma_{MYC}$ de la distribution de l'expression du gène MYC seront utilisées dans notre procédure de simulation.

```{r,echo=F}
rna<-as.numeric(rna.db[,which(colnames(rna.db)=="MYC")])
rna<-rna-mean(rna)
cox.MYC<-linearCox.analyze("MYC",graph=F)$m  #pour estimer beta.hat, le coefficient et H(t).hat, l'estimateur de Nelson Aalen réaliste.
beta<-cox.MYC$coefficients
KM.censor<-survfit(Surv(clinical.db$follow,1-clinical.db$censor)~1) #pour estimer G(t).hat, la loi de la censure réaliste. 
```

### 2.1.2 Etape de simulation 

\begin{algorithm}
\caption{Procédure de simulation et de son étude dans le cas de données distribuées selon un modèle de Cox linéaire. }
\begin{algorithmic} 
\STATE $N=1000$  
\FOR{$k \in \{1, ..., 1000\}$}
  \STATE 
  \STATE \underline{1 : Etape de simulation de $Y_i$,$C_i$,$T_i$ et $\delta_i$ selon les estimations récupérées.}
  \STATE 
  \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de taille $N$ de la variable d'intérêt $X_i$ \end{flushright}
  \STATE $X \sim \mathcal{N}(\mu_{MYC},\sigma_{MYC})$  
  \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de taille $N$ de la variable $T_i$ \end{flushright}
    \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de Yi \end{flushright}
    \STATE $U_i \sim \mathcal{U}[0,1]$  
    \STATE $Y_i = \hat{H^{-1}}(-e^{-\beta*Xi} ln(U_i))$  
    \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de $C_i$ \end{flushright}
    \STATE $U_j \sim \mathcal{U}[0,1]$  
    \STATE $C_i = \hat{G^{-1}}(U_j)$  
    \STATE $T_i = min(Y_i,C_i)$  
  \STATE $\delta_i =  \mathbb{1}_{Y_i<C_i}$
  \STATE 
  \STATE \underline{2 : Etape d'ajustement des modèles à partir de $T_i$,$X_i$ et $\delta_i$.}
  \STATE 
  \STATE $CoxModel_{L}$ = un modèle de Cox linéaire 
  \STATE $CoxModel_{NL3}$ = un modèle de Cox non linéaire de degrés = 3
  \STATE $CoxModel_{NL1}$ = un modèle de Cox non linéaire de degrés = 1
  \STATE 
  \STATE \underline{3 : Calcul et stockage des indicateurs.}  
  \STATE 
  \STATE $AIC_{L}[k] = AIC(CoxModel_{L})$
  \STATE $AIC_{NL3}[k] = AIC(CoxModel_{NL3})$
  \STATE $AIC_{NL1}[k] = AIC(CoxModel_{NL1})$
  \STATE ...
  \STATE 
\ENDFOR
\STATE \begin{flushright} \(\rightarrow\) On représente ensuite graphiquement les distributions des indicateurs récupérés pour les 1000 simulations en fonction du modèle ajusté. \end{flushright}
\end{algorithmic}
\end{algorithm}


```{r,cache=T,echo=F}
#Initialisation 
SL.AIC.l<-numeric(1000)
SL.AIC.nl.3<-numeric(1000)
SL.AIC.nl.1<-numeric(1000)

SL.Conc.l<-numeric(1000)
SL.Conc.nl.3<-numeric(1000)
SL.Conc.nl.1<-numeric(1000)

SL.LRpv.l<-numeric(1000)
SL.LRpv.nl.3<-numeric(1000)
SL.LRpv.nl.1<-numeric(1000)

SL.LRw.l<-numeric(1000)
SL.LRw.nl.3<-numeric(1000)
SL.LRw.nl.1<-numeric(1000)

SL.AOV.l.nl.3<-numeric(1000)
SL.AOV.l.nl.1<-numeric(1000)

mart.rdl.l<-matrix(nrow=1000,ncol=605)
rna.s.mat<-matrix(nrow=1000,ncol=605)

mart.rdl.nl.3<-matrix(nrow=1000,ncol=605)
mart.rdl.nl.1<-matrix(nrow=1000,ncol=605)

CS.time.l<-matrix(nrow=1000,ncol=605)
CS.haz.l<-matrix(nrow=1000,ncol=605)

CS.time.nl.3<-matrix(nrow=1000,ncol=605)
CS.haz.nl.3<-matrix(nrow=1000,ncol=605)

CS.time.nl.1<-matrix(nrow=1000,ncol=605)
CS.haz.nl.1<-matrix(nrow=1000,ncol=605)

ROC.FP.l<-matrix(nrow=1000,ncol=606)
ROC.TP.l<-matrix(nrow=1000,ncol=606)

ROC.FP.nl.3<-matrix(nrow=1000,ncol=606)
ROC.TP.nl.3<-matrix(nrow=1000,ncol=606)

ROC.FP.nl.1<-matrix(nrow=1000,ncol=606)
ROC.TP.nl.1<-matrix(nrow=1000,ncol=606)

for(k in 1:100){
  print(k)
################################################################################### Simulation des données ######################################################################################
N<-605 #taille de l'échantillon simulé 
rna.s<-rna
#rna.s<-rnorm(N,mean(rna),sd(rna)) #variable d'intérêt 
#Simulation de Ti à partir de Yi et Ci 
  #Simulation de Yi 
  Ui<-runif(N)
  Yi.int<--exp(-beta*(rna.s-mean(rna)))*log(Ui)
  #On lit Yi.int sur l'estimation de H(t).hat, l'estimateur de Nelson Aalen réaliste.
  time<-survfit(cox.MYC)$time
  cumhaz<-survfit(cox.MYC)$cumhaz
  
  Yi<-numeric(N)
  #Je calcule H0-1(Yi.int)
  for(i in 1:N){
  if (Yi.int[i] > max(cumhaz)) {Yi[i] <- max(time)
  }else {Yi[i]<-min(time[cumhaz>=Yi.int[i]]) }
  }

  #Simulation de Ci 
  Uj<-runif(N)
  #On lit Uj sur l'estimation de G(t).hat, la loi de la censure réaliste.
  KM.time<-KM.censor$time
  KM.surv<-KM.censor$surv
  Ci<-numeric(N)
  #Je calcule H0-1(Yi.int)
  for(i in 1:N){
   Ci[i]<-min(KM.time[KM.surv<=Uj[i]]) 
  }
Ti=apply(cbind(Yi,Ci),1,min)
Di=as.numeric((Yi<Ci))


############################################################################### Ajustement des modèles #########################################################################################
survie<-Surv(Ti,Di)
#modèle linéaire 
cox.gene.l<-coxph(survie ~ rna.s)
#modèle non linéaire de degré 3
cox.gene.nl.3<-coxph(survie ~ pspline(rna.s,df=0,degree=3))
#modèle non linéaire de degré 1
cox.gene.nl.1<-coxph(survie ~ pspline(rna.s,df=0,degree=1))

########################################################################### Calcul & stockage des indicateurs ##################################################################################

# SL.AIC.l[k]<-AIC(cox.gene.l)
# SL.AIC.nl.3[k]<-AIC(cox.gene.nl.3)
# SL.AIC.nl.1[k]<-AIC(cox.gene.nl.1)
# 
# SL.Conc.l[k]<-concordance(cox.gene.l)$concordance
# SL.Conc.nl.3[k]<-concordance(cox.gene.nl.3)$concordance
# SL.Conc.nl.1[k]<-concordance(cox.gene.nl.1)$concordance
# 
# SL.LRpv.l[k]<-summary(cox.gene.l)$logtest[3]
# SL.LRpv.nl.3[k]<-summary(cox.gene.nl.3)$logtest[3]
# SL.LRpv.nl.1[k]<-summary(cox.gene.nl.1)$logtest[3]
# 
# SL.LRw.l[k]<-1 - pchisq(cox.gene.l$wald.test, df =1)
# SL.LRw.nl.3[k]<-1 - pchisq(cox.gene.nl.3$wald.test, df = cox.gene.nl.3$df)
# SL.LRw.nl.1[k]<-1 - pchisq(cox.gene.nl.1$wald.test, df = cox.gene.nl.1$df)
# 
# SL.AOV.l.nl.3[k]<-anova(cox.gene.l,cox.gene.nl.3)$`P(>|Chi|)`[2]
# SL.AOV.l.nl.1[k]<-anova(cox.gene.l,cox.gene.nl.1)$`P(>|Chi|)`[2]

#stockage des points pour les résidus marintgales
rna.s.mat[k,]<-rna.s[order(rna.s)]

loess_model <- loess(residuals(cox.gene.l,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.l[k,]<-pred$fit[order(rna.s)]


loess_model <- loess(residuals(cox.gene.nl.3,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.nl.3[k,]<-pred$fit[order(rna.s)]


loess_model <- loess(residuals(cox.gene.nl.1,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.nl.1[k,]<-pred$fit[order(rna.s)]

#stockage des points pour les residus cox snell
resid_coxsnell <- -(residuals(cox.gene.l,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.l[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.l[k,1:length(df_base_haz$time)]<-df_base_haz$hazard

resid_coxsnell <- -(residuals(cox.gene.nl.3,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.nl.3[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.nl.3[k,1:length(df_base_haz$time)]<-df_base_haz$hazard

resid_coxsnell <- -(residuals(cox.gene.nl.1,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.nl.1[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.nl.1[k,1:length(df_base_haz$time)]<-df_base_haz$hazard


#stockage pour courbes roc
t<-seq(0,max(Ti),length.out=3)
t<-round(t[-c(1,length(t))],1)
clinical<-data.frame("follow"=Ti,"censor"=Di)
scr<-predict(cox.gene.l,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.l[k,]<-main.roc$FP[,2]
ROC.TP.l[k,]<-main.roc$TP[,2]

scr<-predict(cox.gene.nl.3,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.nl.3[k,]<-main.roc$FP[,2]
ROC.TP.nl.3[k,]<-main.roc$TP[,2]

scr<-predict(cox.gene.nl.3,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.nl.1[k,]<-main.roc$FP[,2]
ROC.TP.nl.1[k,]<-main.roc$TP[,2]

}
```



## 3. Résultats

### Numériques

```{r,fig.width=16,fig.height=11,echo=F,fig.cap="Distributions des indicateurs numériques en fonction du modèle ajusté dans le cas d'une simulation réaliste linéaire."}
par(mfrow=c(2,3))
par(mar = c(5.1, 4.1, 4.1, 2.1))
#Calcul du taux de AICnl<AICl
Tinf.nl1<-round(sum(SL.AIC.nl.1<SL.AIC.l)/length(SL.AIC.l),2)
Tinf.nl3<-round(sum(SL.AIC.nl.3<SL.AIC.l)/length(SL.AIC.l),2)
AIC.df=data.frame("Lin"=SL.AIC.l,"NonLin1"=SL.AIC.nl.1,"NonLin3"=SL.AIC.nl.3)
boxplot(AIC.df,main=paste0("AIC \n Taux de NL1<L = ",Tinf.nl1," et de NL3<L = ",Tinf.nl3),cex.main=1.5,col=c(2,"blue","lightblue"),cex.axis=1.3)


Conc.df=data.frame("Lin"=SL.Conc.l,"NonLin1"=SL.Conc.nl.1,"NonLin3"=SL.Conc.nl.3)
boxplot(Conc.df,col=c(2,"blue","lightblue"),main="Concordance",cex.main=1.5,cex.axis=1.3)

TRaov.NL1<-round(sum(SL.AOV.l.nl.1<=0.05)/length(SL.AOV.l.nl.1),2)
TRaov.NL3<-round(sum(SL.AOV.l.nl.3<=0.05)/length(SL.AOV.l.nl.3),2)
AOV.df=data.frame("L/NonLin1"=SL.AOV.l.nl.1,"L/NonLin3"=SL.AOV.l.nl.3)
boxplot(AOV.df,col=c("blue","lightblue"),cex.main=1.5,main=paste0("Anova p.value \n TR : L/NL1 = ",TRaov.NL1," , L/NL3 = ",TRaov.NL3),cex.axis=1.3)

#calcul du taux de rejet : 
TR.L<-round(sum(SL.LRpv.l<=0.05)/length(SL.LRpv.l),2)
TR.NL1<-round(sum(SL.LRpv.nl.1<=0.05)/length(SL.LRpv.nl.1),2)
TR.NL3<-round(sum(SL.LRpv.nl.3<=0.05)/length(SL.LRpv.nl.3),2)
LRpv.df=data.frame("Lin"=SL.LRpv.l,"NonLin1"=SL.LRpv.nl.1,"NonLin3"=SL.LRpv.nl.3)
boxplot(LRpv.df,col=c(2,"blue","lightblue"),main=paste0("Likelihood Ratio p.value \n TR : L = ",TR.L," , NL1 = ",TR.NL1," , NL3 = ",TR.NL3),cex.main=1.5,cex.axis=1.3)

#calcul du taux de rejet : 
TRw.L<-round(sum(SL.LRw.l<=0.05)/length(SL.LRw.l),2)
TRw.NL1<-round(sum(SL.LRw.nl.1<=0.05)/length(SL.LRw.nl.1),2)
TRw.NL3<-round(sum(SL.LRw.nl.3<=0.05)/length(SL.LRw.nl.3),2)
LRw.df=data.frame("Lin"=SL.LRw.l,"NonLin1"=SL.LRw.nl.1,"NonLin3"=SL.LRw.nl.3)
boxplot(LRw.df,col=c(2,"blue","lightblue"),cex.main=1.5,main=paste0("Wald p.value \n TR : L = ",TRw.L," , NL1 = ",TRw.NL1," , NL3 = ",TRw.NL3),cex.axis=1.3)

```

### Graphiques

```{r,fig.height=16,fig.width=20}
#martingales
par(mfrow=c(3,3))
par(mar = c(5.1, 5.1, 4.1, 2.1))

plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",main="Résidus Martingales",cex.lab=2,cex.main=2)
legend("top",cex=1.5,legend = c("Cox linear","Cox non linear d=1","Cox non linear d=3"),col=c("red","blue","lightblue"),lty=c(1,1,1))

for(i in 2:100){
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
}


plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),main="Courbes ROC",cex.lab=2,cex.main=2)


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
}

plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',main="Résidus Cox-Snell",ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')

}


plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat[1,],mart.rdl.nl.1[1,],type='l',col='blue')

for(i in 2:100){
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
points(rna.s.mat[i,],mart.rdl.nl.1[i,],type='l',col='blue')
}


plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.1[1,],ROC.TP.nl.1[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
  points(ROC.FP.nl.1[i,],ROC.TP.nl.1[i,],type='s',col='blue')
}

plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.3[1,],CS.haz.nl.3[1,],type='l',col='lightblue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')
  points(CS.time.nl.1[i,],CS.haz.nl.1[i,],type='l',col='blue')

}



plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat[1,],mart.rdl.nl.3[1,],type='l',col='lightblue')
points(rna.s.mat[1,],mart.rdl.nl.1[1,],type='l',col='blue')


for(i in 2:100){
  
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
points(rna.s.mat[i,],mart.rdl.nl.1[i,],type='l',col='blue')
points(rna.s.mat[i,],mart.rdl.nl.3[i,],type='l',col='lightblue')
 
}


plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.3[1,],ROC.TP.nl.3[1,],type='s',col='lightblue')
points(ROC.FP.nl.1[1,],ROC.TP.nl.1[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
  points(ROC.FP.nl.1[i,],ROC.TP.nl.1[i,],type='s',col='blue')
  points(ROC.FP.nl.3[i,],ROC.TP.nl.3[i,],type='s',col='lightblue')
  
}

plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.3[1,],CS.haz.nl.3[1,],type='l',col='lightblue')
points(CS.time.nl.1[1,],CS.haz.nl.1[1,],type='l',col='blue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')
  points(CS.time.nl.1[i,],CS.haz.nl.1[i,],type='l',col='blue')
  points(CS.time.nl.3[i,],CS.haz.nl.3[i,],type='l',col='lightblue')
  
}


#CS



```


# 3. Etude de simulation de données réalistes non linéaire à partir de l'expression du gène TP53

## 3.1 Explication de la procédure

### 3.1.1 Etape d'estimation pré-simulation réaliste 


Comme nous l'avons fait dans le cas linéaire, afin de pouvoir simuler des données selon un cox linéaire réaliste pour notre contexte. Nous allons préalablement estimé : 

- un modèle de cox non linéaire de degré 3 à partir des données du gène TP53 qui semble présenter un comportement non linéaire relativement à la significativité de la p.value associée au modèle non linéaire (pv = 0.1344) et à l'inverse une p.value > 0.05 dans le cas linéaire. Ce modèle nous permet de récupérer l'estimation d'une fonction $\hat{f}(X)$ décrivant le comportement du risque en fonction de l'expression du gène et l'estimation de la fonction de risque cumulé $\hat{H}(t)$ selon Nelson-Aalen. 

- la loi de la censure $C_i$ à partir d'un Kaplan-Meier. Cela nous permet de récupérer l'estimation de la fonction $\hat{G}(t)$. 

Ces estimations ainsi que la moyenne $\mu_{TP53}$ et l'écart-type $\sigma_{TP53}$ de la distribution de l'expression du gène TP53 seront utilisées dans notre procédure de simulation.

```{r,echo=F,fig.width=8,fig.height=7}
rna<-as.numeric(rna.db[,which(colnames(rna.db)=="TP53")])
rna<-rna-mean(rna)
cox.TP53<-nonlinearCox.analyze("TP53",graph=F)$m  #pour estimer beta.hat, le coefficient et H(t).hat, l'estimateur de Nelson Aalen réaliste.

#afficher le log risk 
pred<-predict(cox.TP53,type="risk",se=T)
plot(rna,log(pred$fit),ylab="log(risk)",cex.main=1,col='blue',cex.main=1,main="Comparaison entre le log(Risk) réel et le log(Risk) estimé",lwd=3)
rna.c<-rna
rna2=rna.c^2

est.r<-lm(pred$fit~rna.c+rna2)
fx<-log(est.r$coefficients[2]*rna.c+est.r$coefficients[3]*rna2+est.r$coefficients[1])
points(sort(rna),fx[order(rna)],col='red',type='l',lwd=2)
legend("topright",legend=c("observations","estimation de f(x)"),col=c('blue','red'),pch=19)

KM.censor<-survfit(Surv(clinical.db$follow,1-clinical.db$censor)~1) #pour estimer G(t).hat, la loi de la censure réaliste. 
```

### 3.1.2 Etape de simulation 

\begin{algorithm}
\caption{Procédure de simulation et de son étude dans le cas de données distribuées selon un modèle de Cox non linéaire. }
\begin{algorithmic} 
\STATE $N=1000$  
\FOR{$k \in \{1, ..., 1000\}$}
  \STATE 
  \STATE \underline{1 : Etape de simulation de $Y_i$,$C_i$,$T_i$ et $\delta_i$ selon les estimations récupérées.}
  \STATE 
  \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de taille $N$ de la variable d'intérêt $X_i$ \end{flushright}
  \STATE $X \sim \mathcal{N}(\mu_{TP53},\sigma_{TP53})$  
  \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de taille $N$ de la variable $T_i$ \end{flushright}
    \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de Yi \end{flushright}
    \STATE $U_i \sim \mathcal{U}[0,1]$  
    \STATE $Y_i = \hat{H^{-1}}(-e^{-\hat{f}(X_i)} ln(U_i))$  
    \STATE \begin{flushright} \(\rightarrow\) Simulation d'un echantillon de $C_i$ \end{flushright}
    \STATE $U_j \sim \mathcal{U}[0,1]$  
    \STATE $C_i = \hat{G^{-1}}(U_j)$  
    \STATE $T_i = min(Y_i,C_i)$  
  \STATE $\delta_i =  \mathbb{1}_{Y_i<C_i}$
  \STATE 
  \STATE \underline{2 : Etape d'ajustement des modèles à partir de $T_i$,$X_i$ et $\delta_i$.}
  \STATE 
  \STATE $CoxModel_{L}$ = un modèle de Cox linéaire 
  \STATE $CoxModel_{NL3}$ = un modèle de Cox non linéaire de degrés = 3
  \STATE $CoxModel_{NL1}$ = un modèle de Cox non linéaire de degrés = 1
  \STATE 
  \STATE \underline{3 : Calcul et stockage des indicateurs.}  
  \STATE 
  \STATE $AIC_{L}[k] = AIC(CoxModel_{L})$
  \STATE $AIC_{NL3}[k] = AIC(CoxModel_{NL3})$
  \STATE $AIC_{NL1}[k] = AIC(CoxModel_{NL1})$
  \STATE ...
  \STATE 
\ENDFOR
\STATE \begin{flushright} \(\rightarrow\) On représente ensuite graphiquement les distributions des indicateurs récupérés pour les 1000 simulations en fonction du modèle ajusté. \end{flushright}
\end{algorithmic}
\end{algorithm}

```{r,cache=T,echo=F}
#Initialisation 
SNL.AIC.l<-numeric(1000)
SNL.AIC.nl.3<-numeric(1000)
SNL.AIC.nl.1<-numeric(1000)

SNL.Conc.l<-numeric(1000)
SNL.Conc.nl.3<-numeric(1000)
SNL.Conc.nl.1<-numeric(1000)

SNL.LRpv.l<-numeric(1000)
SNL.LRpv.nl.3<-numeric(1000)
SNL.LRpv.nl.1<-numeric(1000)

SNL.LRw.l<-numeric(1000)
SNL.LRw.nl.3<-numeric(1000)
SNL.LRw.nl.1<-numeric(1000)

SNL.AOV.l.nl.3<-numeric(1000)
SNL.AOV.l.nl.1<-numeric(1000)


mart.rdl.l2<-matrix(nrow=1000,ncol=605)
rna.s.mat2<-matrix(nrow=1000,ncol=605)

mart.rdl.nl.32<-matrix(nrow=1000,ncol=605)
mart.rdl.nl.12<-matrix(nrow=1000,ncol=605)

CS.time.l2<-matrix(nrow=1000,ncol=605)
CS.haz.l2<-matrix(nrow=1000,ncol=605)

CS.time.nl.32<-matrix(nrow=1000,ncol=605)
CS.haz.nl.32<-matrix(nrow=1000,ncol=605)

CS.time.nl.12<-matrix(nrow=1000,ncol=605)
CS.haz.nl.12<-matrix(nrow=1000,ncol=605)

ROC.FP.l2<-matrix(nrow=1000,ncol=606)
ROC.TP.l2<-matrix(nrow=1000,ncol=606)

ROC.FP.nl.32<-matrix(nrow=1000,ncol=606)
ROC.TP.nl.32<-matrix(nrow=1000,ncol=606)

ROC.FP.nl.12<-matrix(nrow=1000,ncol=606)
ROC.TP.nl.12<-matrix(nrow=1000,ncol=606)

for(k in 1:100){
  print(k)
################################################################################### Simulation des données ######################################################################################
N<-605 #taille de l'échantillon simulé 
rna.s<-rna.c
#rna.s<-rnorm(N,mean(rna),sd(rna)) #variable d'intérêt 
#Simulation de Ti à partir de Yi et Ci 
  #Simulation de Yi 
  Ui<-runif(N)
  f.x=est.r$coefficients[2]*(rna.s)+est.r$coefficients[3]*(rna.s)^2+est.r$coefficients[1]
  f.m=est.r$coefficients[2]*(mean(rna))+est.r$coefficients[3]*(mean(rna))^2+est.r$coefficients[1]
  Yi.int<--exp(-(f.x))*log(Ui)
  
  #On lit Yi.int sur l'estimation de H(t).hat, l'estimateur de Nelson Aalen réaliste.
  time<-survfit(cox.TP53)$time
  cumhaz<-survfit(cox.TP53)$cumhaz
  Yi<-numeric(N)
  #Je calcule H0-1(Yi.int)
  for(i in 1:N){
  if (Yi.int[i] > max(cumhaz)) {Yi[i] <- max(time)
  }else {Yi[i]<-min(time[cumhaz>=Yi.int[i]]) }
  }

  #Simulation de Ci 
  Uj<-runif(N)
  #On lit Uj sur l'estimation de G(t).hat, la loi de la censure réaliste.
  KM.time<-KM.censor$time
  KM.surv<-KM.censor$surv
  Ci<-numeric(N)
  #Je calcule H0-1(Yi.int)
  for(i in 1:N){
   Ci[i]<-min(KM.time[KM.surv<=Uj[i]]) 
  }
Ti=apply(cbind(Yi,Ci),1,min)
Di=as.numeric((Yi<Ci))


############################################################################### Ajustement des modèles #########################################################################################
survie<-Surv(Ti,Di)
#modèle linéaire 
cox.gene.l<-coxph(survie ~ rna.s)
#modèle non linéaire de degré 3
cox.gene.nl.3<-coxph(survie ~ pspline(rna.s,df=0,degree=3))
#modèle non linéaire de degré 1
cox.gene.nl.1<-coxph(survie ~ pspline(rna.s,df=0,degree=1))

########################################################################### Calcul & stockage des indicateurs ##################################################################################

# SNL.AIC.l[k]<-AIC(cox.gene.l)
# SNL.AIC.nl.3[k]<-AIC(cox.gene.nl.3)
# SNL.AIC.nl.1[k]<-AIC(cox.gene.nl.1)
# 
# SNL.Conc.l[k]<-concordance(cox.gene.l)$concordance
# SNL.Conc.nl.3[k]<-concordance(cox.gene.nl.3)$concordance
# SNL.Conc.nl.1[k]<-concordance(cox.gene.nl.1)$concordance
# 
# SNL.LRpv.l[k]<-summary(cox.gene.l)$logtest[3]
# SNL.LRpv.nl.3[k]<-summary(cox.gene.nl.3)$logtest[3]
# SNL.LRpv.nl.1[k]<-summary(cox.gene.nl.1)$logtest[3]
# 
# SNL.LRw.l[k]<-1 - pchisq(cox.gene.l$wald.test, df =1)
# SNL.LRw.nl.3[k]<-1 - pchisq(cox.gene.nl.3$wald.test, df = cox.gene.nl.3$df)
# SNL.LRw.nl.1[k]<-1 - pchisq(cox.gene.nl.1$wald.test, df = cox.gene.nl.1$df)
# 
# SNL.AOV.l.nl.3[k]<-anova(cox.gene.l,cox.gene.nl.3)$`P(>|Chi|)`[2]
# SNL.AOV.l.nl.1[k]<-anova(cox.gene.l,cox.gene.nl.1)$`P(>|Chi|)`[2]

# #stockage des points pour les résidus marintgales
rna.s.mat2[k,]<-rna.s[order(rna.s)]

loess_model <- loess(residuals(cox.gene.l,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.l2[k,]<-pred$fit[order(rna.s)]


loess_model <- loess(residuals(cox.gene.nl.3,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.nl.32[k,]<-pred$fit[order(rna.s)]


loess_model <- loess(residuals(cox.gene.nl.1,type="martingale") ~ rna.s)
pred<-predict(loess_model,newdata=rna.s,se=T)
mart.rdl.nl.12[k,]<-pred$fit[order(rna.s)]

#stockage des points pour les residus cox snell
resid_coxsnell <- -(residuals(cox.gene.l,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.l2[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.l2[k,1:length(df_base_haz$time)]<-df_base_haz$hazard

resid_coxsnell <- -(residuals(cox.gene.nl.3,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.nl.32[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.nl.32[k,1:length(df_base_haz$time)]<-df_base_haz$hazard

resid_coxsnell <- -(residuals(cox.gene.nl.1,type="martingale") - Di)
fit_coxsnell <- coxph(formula = Surv(resid_coxsnell, Di) ~ 1,
                      ties    = c("efron","breslow","exact")[1])
df_base_haz <- basehaz(fit_coxsnell, centered = FALSE)
CS.time.nl.12[k,1:length(df_base_haz$time)]<-df_base_haz$time
CS.haz.nl.12[k,1:length(df_base_haz$time)]<-df_base_haz$hazard


#stockage pour courbes roc
t<-seq(0,max(Ti),length.out=3)
t<-round(t[-c(1,length(t))],1)
clinical<-data.frame("follow"=Ti,"censor"=Di)
scr<-predict(cox.gene.l,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.l2[k,]<-main.roc$FP[,2]
ROC.TP.l2[k,]<-main.roc$TP[,2]

scr<-predict(cox.gene.nl.3,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.nl.32[k,]<-main.roc$FP[,2]
ROC.TP.nl.32[k,]<-main.roc$TP[,2]

scr<-predict(cox.gene.nl.3,type='lp')
main.roc<-timeROC(clinical$follow,delta=clinical$censor,marker=scr,times=t,cause=1,iid=T)
ROC.FP.nl.12[k,]<-main.roc$FP[,2]
ROC.TP.nl.12[k,]<-main.roc$TP[,2]

}
```

## 3.2 Résultats

### Numériques 

```{r,fig.width=16,fig.height=11,echo=F,fig.cap="Distributions des indicateurs numériques en fonction du modèle ajusté dans le cas d'une simulation réaliste non linéaire."}
par(mfrow=c(2,3))
par(mar = c(5.1, 4.1, 4.1, 2.1))
#Calcul du taux de AICnl<AICl
Tinf.nl1<-round(sum(SNL.AIC.nl.1<SNL.AIC.l)/length(SNL.AIC.l),2)
Tinf.nl3<-round(sum(SNL.AIC.nl.3<SNL.AIC.l)/length(SNL.AIC.l),2)
AIC.df=data.frame("Lin"=SNL.AIC.l,"NonLin1"=SNL.AIC.nl.1,"NonLin3"=SNL.AIC.nl.3)
boxplot(AIC.df,main=paste0("AIC \n Taux de NL1<L = ",Tinf.nl1," et de NL3<L = ",Tinf.nl3),cex.main=1.5,col=c(2,"blue","lightblue"),cex.axis=1.3)


Conc.df=data.frame("Lin"=SNL.Conc.l,"NonLin1"=SNL.Conc.nl.1,"NonLin3"=SNL.Conc.nl.3)
boxplot(Conc.df,col=c(2,"blue","lightblue"),main="Concordance",cex.main=1.5,cex.axis=1.3)

TRaov.NL1<-round(sum(SNL.AOV.l.nl.1<=0.05)/length(SNL.AOV.l.nl.1),2)
TRaov.NL3<-round(sum(SNL.AOV.l.nl.3<=0.05)/length(SNL.AOV.l.nl.3),2)
AOV.df=data.frame("L/NonLin1"=SNL.AOV.l.nl.1,"L/NonLin3"=SNL.AOV.l.nl.3)
boxplot(AOV.df,col=c("blue","lightblue"),cex.main=1.5,main=paste0("Anova p.value \n TR : L/NL1 = ",TRaov.NL1," , L/NL3 = ",TRaov.NL3),cex.axis=1.3)

#calcul du taux de rejet : 
TR.L<-round(sum(SNL.LRpv.l<=0.05)/length(SNL.LRpv.l),2)
TR.NL1<-round(sum(SNL.LRpv.nl.1<=0.05)/length(SNL.LRpv.nl.1),2)
TR.NL3<-round(sum(SNL.LRpv.nl.3<=0.05)/length(SNL.LRpv.nl.3),2)
LRpv.df=data.frame("Lin"=SNL.LRpv.l,"NonLin1"=SNL.LRpv.nl.1,"NonLin3"=SNL.LRpv.nl.3)
boxplot(LRpv.df,col=c(2,"blue","lightblue"),main=paste0("Likelihood Ratio p.value \n TR : L = ",TR.L," , NL1 = ",TR.NL1," , NL3 = ",TR.NL3),cex.main=1.5,cex.axis=1.3)

#calcul du taux de rejet : 
TRw.L<-round(sum(SNL.LRw.l<=0.05)/length(SNL.LRw.l),2)
TRw.NL1<-round(sum(SNL.LRw.nl.1<=0.05)/length(SNL.LRw.nl.1),2)
TRw.NL3<-round(sum(SNL.LRw.nl.3<=0.05)/length(SNL.LRw.nl.3),2)
LRw.df=data.frame("Lin"=SNL.LRw.l,"NonLin1"=SNL.LRw.nl.1,"NonLin3"=SNL.LRw.nl.3)
boxplot(LRw.df,col=c(2,"blue","lightblue"),cex.main=1.5,main=paste0("Wald p.value \n TR : L = ",TRw.L," , NL1 = ",TRw.NL1," , NL3 = ",TRw.NL3),cex.axis=1.3)


```



### Graphiques 

```{r,fig.height=16,fig.width=20}
#martingales
par(mfrow=c(3,3))
par(mar = c(5.1, 5.1, 4.1, 2.1))

plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",main="Résidus Martingales",cex.lab=2,cex.main=2)
legend("bottomleft",cex=1.5,legend = c("Cox linear","Cox non linear d=1","Cox non linear d=3"),col=c("red","blue","lightblue"),lty=c(1,1,1))

for(i in 2:100){
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
}


plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),main="Courbes ROC",cex.lab=2,cex.main=2)


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',main="Résidus Cox-Snell",ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')

}


plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat2[1,],mart.rdl.nl.12[1,],type='l',col='blue')

for(i in 2:100){
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
points(rna.s.mat2[i,],mart.rdl.nl.12[i,],type='l',col='blue')
}


plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.12[1,],ROC.TP.nl.12[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
  points(ROC.FP.nl.12[i,],ROC.TP.nl.12[i,],type='s',col='blue')
}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.32[1,],CS.haz.nl.32[1,],type='l',col='lightblue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')
  points(CS.time.nl.12[i,],CS.haz.nl.12[i,],type='l',col='blue')

}



plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat2[1,],mart.rdl.nl.32[1,],type='l',col='lightblue')
points(rna.s.mat2[1,],mart.rdl.nl.12[1,],type='l',col='blue')


for(i in 2:100){
  
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
points(rna.s.mat2[i,],mart.rdl.nl.12[i,],type='l',col='blue')
points(rna.s.mat2[i,],mart.rdl.nl.32[i,],type='l',col='lightblue')
 
}


plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.32[1,],ROC.TP.nl.32[1,],type='s',col='lightblue')
points(ROC.FP.nl.12[1,],ROC.TP.nl.12[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
  points(ROC.FP.nl.12[i,],ROC.TP.nl.12[i,],type='s',col='blue')
  points(ROC.FP.nl.32[i,],ROC.TP.nl.32[i,],type='s',col='lightblue')
  
}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.32[1,],CS.haz.nl.32[1,],type='l',col='lightblue')
points(CS.time.nl.12[1,],CS.haz.nl.12[1,],type='l',col='blue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')
  points(CS.time.nl.12[i,],CS.haz.nl.12[i,],type='l',col='blue')
  points(CS.time.nl.32[i,],CS.haz.nl.32[i,],type='l',col='lightblue')
  
}


```


[Production graphique par indicateurs pour le rapport]

```{r,fig.height=16,fig.width=14}
#martingales 
par(mfrow=c(3,2))
par(mar = c(5.1, 5.1, 4.1, 2.1))

plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",main="Linéaire",cex.lab=2,cex.main=2)
legend("top",cex=1.5,legend = c("Cox linear","Cox non linear d=1","Cox non linear d=3"),col=c("red","blue","lightblue"),lty=c(1,1,1))

for(i in 2:100){
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
}

plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",main="Non linéaire",cex.lab=2,cex.main=2)

for(i in 2:100){
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
}


plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat[1,],mart.rdl.nl.1[1,],type='l',col='blue')

for(i in 2:100){
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
points(rna.s.mat[i,],mart.rdl.nl.1[i,],type='l',col='blue')
}

plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat2[1,],mart.rdl.nl.12[1,],type='l',col='blue')

for(i in 2:100){
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
points(rna.s.mat2[i,],mart.rdl.nl.12[i,],type='l',col='blue')
}


plot(rna.s.mat[1,],mart.rdl.l[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat[1,],mart.rdl.nl.3[1,],type='l',col='lightblue')
points(rna.s.mat[1,],mart.rdl.nl.1[1,],type='l',col='blue')


for(i in 2:100){
  
points(rna.s.mat[i,],mart.rdl.l[i,],type='l',col='red')
points(rna.s.mat[i,],mart.rdl.nl.1[i,],type='l',col='blue')
points(rna.s.mat[i,],mart.rdl.nl.3[i,],type='l',col='lightblue')
 
}


plot(rna.s.mat2[1,],mart.rdl.l2[1,],type='l',col='red',ylim=c(-0.5,0.5),ylab="Résidus martingales",xlab="rna",cex.lab=2)
points(rna.s.mat2[1,],mart.rdl.nl.32[1,],type='l',col='lightblue')
points(rna.s.mat2[1,],mart.rdl.nl.12[1,],type='l',col='blue')


for(i in 2:100){
  
points(rna.s.mat2[i,],mart.rdl.l2[i,],type='l',col='red')
points(rna.s.mat2[i,],mart.rdl.nl.12[i,],type='l',col='blue')
points(rna.s.mat2[i,],mart.rdl.nl.32[i,],type='l',col='lightblue')
 
}


#Courbes ROC 
par(mfrow=c(3,2))
par(mar = c(5.1, 5.1, 4.1, 2.1))


plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),main="Linéaire",cex.lab=2,cex.main=2)


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
}

plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),main="Non Linéaire",cex.lab=2,cex.main=2)


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
}



plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.1[1,],ROC.TP.nl.1[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
  points(ROC.FP.nl.1[i,],ROC.TP.nl.1[i,],type='s',col='blue')
}

plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.12[1,],ROC.TP.nl.12[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
  points(ROC.FP.nl.12[i,],ROC.TP.nl.12[i,],type='s',col='blue')
}

plot(ROC.FP.l[1,],ROC.TP.l[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.3[1,],ROC.TP.nl.3[1,],type='s',col='lightblue')
points(ROC.FP.nl.1[1,],ROC.TP.nl.1[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l[i,],ROC.TP.l[i,],type='s',col='red')
  points(ROC.FP.nl.1[i,],ROC.TP.nl.1[i,],type='s',col='blue')
  points(ROC.FP.nl.3[i,],ROC.TP.nl.3[i,],type='s',col='lightblue')
  
}

plot(ROC.FP.l2[1,],ROC.TP.l2[1,],type='s',col='red',xlab="FP",ylab="TP",ylim=c(0,1),xlim=c(0,1),cex.lab=2)
points(ROC.FP.nl.32[1,],ROC.TP.nl.32[1,],type='s',col='lightblue')
points(ROC.FP.nl.12[1,],ROC.TP.nl.12[1,],type='s',col='blue')


for(i in 2:100){
  
  points(ROC.FP.l2[i,],ROC.TP.l2[i,],type='s',col='red')
  points(ROC.FP.nl.12[i,],ROC.TP.nl.12[i,],type='s',col='blue')
  points(ROC.FP.nl.32[i,],ROC.TP.nl.32[i,],type='s',col='lightblue')
  
}

#CS

par(mfrow=c(3,2))
par(mar = c(5.1, 5.1, 4.1, 2.1))

plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',main="Linéaire",ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')

}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',main="Non linéaire",ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')

}


plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.3[1,],CS.haz.nl.3[1,],type='l',col='lightblue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')
  points(CS.time.nl.1[i,],CS.haz.nl.1[i,],type='l',col='blue')

}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.32[1,],CS.haz.nl.32[1,],type='l',col='lightblue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')
  points(CS.time.nl.12[i,],CS.haz.nl.12[i,],type='l',col='blue')

}


plot(CS.time.l[1,],CS.haz.l[1,],type='l',col='red',ylab="",ylim=c(0,3),xlim=c(0,3),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.3[1,],CS.haz.nl.3[1,],type='l',col='lightblue')
points(CS.time.nl.1[1,],CS.haz.nl.1[1,],type='l',col='blue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l[i,],CS.haz.l[i,],type='l',col='red')
  points(CS.time.nl.1[i,],CS.haz.nl.1[i,],type='l',col='blue')
  points(CS.time.nl.3[i,],CS.haz.nl.3[i,],type='l',col='lightblue')
  
}

plot(CS.time.l2[1,],CS.haz.l2[1,],type='l',col='red',ylab="",ylim=c(0,5),xlim=c(0,5),cex.lab=2,cex.main=2,xlab="Résidus Cox-Snell")
points(CS.time.nl.32[1,],CS.haz.nl.32[1,],type='l',col='lightblue')
points(CS.time.nl.12[1,],CS.haz.nl.12[1,],type='l',col='blue')
abline(a=0,b=1,lty=2)


for(i in 2:100){
  
  points(CS.time.l2[i,],CS.haz.l2[i,],type='l',col='red')
  points(CS.time.nl.12[i,],CS.haz.nl.12[i,],type='l',col='blue')
  points(CS.time.nl.32[i,],CS.haz.nl.32[i,],type='l',col='lightblue')
  
}


```

```{r,echo=F,eval=F}


# Brier score
# 
cox.gene.l2<-coxph(survie ~ rna.s,x=TRUE)
#modèle non linéaire de degré 3
cox.gene.nl.32<-coxph(survie ~ pspline(rna.s,df=0,degree=3),x=TRUE)
#modèle non linéaire de degré 1
cox.gene.nl.12<-coxph(survie ~ pspline(rna.s,df=0,degree=1),x=TRUE)
db<-data.frame(Ti,Di,rna.s)
library(pec)
cf2=pec(list("Cox linear"=cox.gene.l2,"Cox non linear 3"=cox.gene.nl.32,"Cox non linear 1"=cox.gene.nl.12),
    type="risk",formula=Surv(Ti,Di)~1,
    data=db,B=100,splitMethod = "CvBoot",reference=T,exact = T)
plot(cf2)
iBs<-crps(cf2)[,1]
text(x=c(5,5,5,5),y=c(0.08,0.06,0.04,0.02),labels=c(paste0("IBSc = ",round(iBs[1],4)),paste0("IBSc = ",round(iBs[2],4)),paste0("IBSc = ",round(iBs[3],4)),paste0("IBSc = ",round(iBs[4],4))),col=c('black','red','green','blue'),cex=rep(2,4))

```
