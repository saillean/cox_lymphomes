## Q-Q plot : procédure reprise depuis le début 

##On simule des données linéaire : 
############################################################################################################################################################
#################################################################### SIMULATION LINEAIRE  ##################################################################
############################################################################################################################################################

n.obs=605
#Simulation de Xi (irl rna)
Xi<-sort(rnorm(n.obs))
#On centre les Xi 
Xi<-Xi-mean(Xi)

beta<-3
#Simulation de Ti (irl follow) & deltai
### Simulation de Yi 
U<-runif(n.obs)
Yi<--log(U)/exp(beta*Xi)
### Simulation de Ci 
lambda.c=5.5
Ci<-rexp(n.obs,lambda.c)

Ti<-apply(cbind(Yi,Ci),1,min)

Di<-(Yi<Ci)

#On vérifie le taux de censure qui doit etre proche de 0.3

sum(Di)/n.obs #ok ! 

# On a des données linéaires simulées selon un Cox linéaire ! 

#################################################################### ESTIMATION CENSURE ##################################################################
#On estime Sc
delta.inv<-1-Di
KM.censor<-survfit(Surv(Ti,delta.inv)~1)
plot(KM.censor,main="Estimateur de KM de la loi de la censure C")
survie<-Surv(Ti,Di)

#################################################################### ESTIMATION linéaire ##################################################################

#On estime beta et H0 pour chaque modèle 
cox.gene.lin<-coxph(survie~Xi)
H0.lin <- survfit(cox.gene.lin)
# Plot cumulative hazard function pour vérifier l'allure linéaire de H0 
plot(H0.lin, fun = "cumhaz", xlab = "Temps", ylab = "Fonction de risques cumulés pour le linéaire",lwd=2,col='red',main="Nelson Aalen estimator")

# On a le beta estimé, HO estimé et la loi de la censure estimé pour un modèle linéaire ! 

######################################################## CALIBRATION linéaire  ########################################################################

#On verifie que beta*X est bien équivalent au log(Risk)
round(log(predict(cox.gene.lin,type="risk")),4)==round(cox.gene.lin$coefficients*Xi,4)
plot(Xi,log(predict(cox.gene.lin,type="risk")))
plot(Xi,cox.gene.lin$coefficients*Xi)

log.risk<-log(predict(cox.gene.lin,type="risk"))
Qi.lin<-numeric(n.obs)

for(i in 1:n.obs){
  time.i<-Ti[i]
  #On calcule SY(Ti)
  time<-H0.lin$time
  cumhaz<-H0.lin$cumhaz
  if (time.i > max(time)){  H0.Ti <- max(cumhaz) 
  }else H0.Ti<-min(cumhaz[time>=time.i])
  
  SY.Ti<-exp(-exp(log.risk[i])*H0.Ti)
  
  #On calcule SC(Ti)
  time<-KM.censor$time
  surv<-KM.censor$surv
  if (time.i > max(time)){ SC.Ti <- min(surv) 
  }else SC.Ti<-max(surv[time>=time.i])
  
  Qi.lin[i]<-SY.Ti*SC.Ti
}

#Représentation graphique 
plot((1:length(Qi.lin))/length(Qi.lin),sort(Qi.lin),col="red",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
abline(a=0,b=1,lty=2)

################################################################### ESTIMATION non linéaire ##################################################################

#On estime beta et H0 pour chaque modèle 
cox.gene.nl<-coxph(survie~pspline(Xi,df=0))
H0.nl <- survfit(cox.gene.nl)
# Plot cumulative hazard function pour vérifier l'allure linéaire de H0 
plot(H0.nl, fun = "cumhaz", xlab = "Temps", ylab = "Fonction de risques cumulés pour le linéaire",lwd=2,col='red',main="Nelson Aalen estimator")

# On a le beta estimé, HO estimé et la loi de la censure estimé pour un modèle linéaire ! 

######################################################## CALIBRATION linéaire  ########################################################################

#On verifie que beta*X est bien équivalent au log(Risk)
round(log(predict(cox.gene.nl,type="risk")),4)==round(cox.gene.nl$coefficients*Xi,4)
plot(Xi,log(predict(cox.gene.nl,type="risk")))
plot(Xi,cox.gene.nl$coefficients*Xi)

log.risk<-log(predict(cox.gene.nl,type="risk"))
Qi.nl<-numeric(n.obs)

for(i in 1:n.obs){
  time.i<-Ti[i]
  #On calcule SY(Ti)
  time<-H0.nl$time
  cumhaz<-H0.nl$cumhaz
  if (time.i > max(time)){  H0.Ti <- max(cumhaz) 
  }else H0.Ti<-min(cumhaz[time>=time.i])
  
  SY.Ti<-exp(-exp(log.risk[i])*H0.Ti)
  
  #On calcule SC(Ti)
  time<-KM.censor$time
  surv<-KM.censor$surv
  if (time.i > max(time)){ SC.Ti <- min(surv) 
  }else SC.Ti<-max(surv[time>=time.i])
  
  Qi.nl[i]<-SY.Ti*SC.Ti
}

#Représentation graphique 
plot((1:length(Qi.lin))/length(Qi.lin),sort(Qi.lin),col="red",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
points((1:length(Qi.nl))/length(Qi.nl),sort(Qi.nl),col="blue",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
abline(a=0,b=1,lty=2)



##On simule des données non linéaire : 
############################################################################################################################################################
################################################################## SIMULATION NON LINEAIRE  ################################################################
############################################################################################################################################################

n.obs=605
#Simulation de Xi (irl rna)
Xi<-sort(rnorm(n.obs))
#On centre les Xi 
Xi<-Xi-mean(Xi)

beta<-3
#Simulation de Ti (irl follow) & deltai
### Simulation de Yi 
U<-runif(n.obs)
Yi<--log(U)/exp(beta*cos((Xi)*pi))
### Simulation de Ci 
lambda.c=5.5
Ci<-rexp(n.obs,lambda.c)

Ti<-apply(cbind(Yi,Ci),1,min)

Di<-(Yi<Ci)

#On vérifie le taux de censure qui doit etre proche de 0.3

sum(Di)/n.obs #ok ! 

# On a des données linéaires simulées selon un Cox linéaire ! 

#################################################################### ESTIMATION CENSURE ##################################################################
#On estime Sc
delta.inv<-1-Di
KM.censor<-survfit(Surv(Ti,delta.inv)~1)
plot(KM.censor,main="Estimateur de KM de la loi de la censure C")
survie<-Surv(Ti,Di)

#################################################################### ESTIMATION linéaire ##################################################################

#On estime beta et H0 pour chaque modèle 
cox.gene.lin<-coxph(survie~Xi)
H0.lin <- survfit(cox.gene.lin)
# Plot cumulative hazard function pour vérifier l'allure linéaire de H0 
plot(H0.lin, fun = "cumhaz", xlab = "Temps", ylab = "Fonction de risques cumulés pour le linéaire",lwd=2,col='red',main="Nelson Aalen estimator")

# On a le beta estimé, HO estimé et la loi de la censure estimé pour un modèle linéaire ! 

######################################################## CALIBRATION linéaire  ########################################################################

#On verifie que beta*X est bien équivalent au log(Risk)
round(log(predict(cox.gene.lin,type="risk")),4)==round(cox.gene.lin$coefficients*Xi,4)
plot(Xi,log(predict(cox.gene.lin,type="risk")))
plot(Xi,cox.gene.lin$coefficients*Xi)

log.risk<-log(predict(cox.gene.lin,type="risk"))
Qi.lin<-numeric(n.obs)

for(i in 1:n.obs){
  time.i<-Ti[i]
  #On calcule SY(Ti)
  time<-H0.lin$time
  cumhaz<-H0.lin$cumhaz
  if (time.i > max(time)){  H0.Ti <- max(cumhaz) 
  }else H0.Ti<-min(cumhaz[time>=time.i])
  
  SY.Ti<-exp(-exp(log.risk[i])*H0.Ti)
  
  #On calcule SC(Ti)
  time<-KM.censor$time
  surv<-KM.censor$surv
  if (time.i > max(time)){ SC.Ti <- min(surv) 
  }else SC.Ti<-max(surv[time>=time.i])
  
  Qi.lin[i]<-SY.Ti*SC.Ti
}

#Représentation graphique 
plot((1:length(Qi.lin))/length(Qi.lin),sort(Qi.lin),col="red",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
abline(a=0,b=1,lty=2)

################################################################### ESTIMATION non linéaire ##################################################################

#On estime beta et H0 pour chaque modèle 
cox.gene.nl<-coxph(survie~pspline(Xi,df=0))
H0.nl <- survfit(cox.gene.nl)
# Plot cumulative hazard function pour vérifier l'allure linéaire de H0 
plot(H0.nl, fun = "cumhaz", xlab = "Temps", ylab = "Fonction de risques cumulés pour le linéaire",lwd=2,col='red',main="Nelson Aalen estimator")

# On a le beta estimé, HO estimé et la loi de la censure estimé pour un modèle linéaire ! 

######################################################## CALIBRATION linéaire  ########################################################################

#On verifie que beta*X est bien équivalent au log(Risk)
round(log(predict(cox.gene.nl,type="risk")),4)==round(cox.gene.nl$coefficients*Xi,4)
plot(Xi,log(predict(cox.gene.nl,type="risk")))
plot(Xi,cox.gene.nl$coefficients*Xi)

log.risk<-log(predict(cox.gene.nl,type="risk"))
Qi.nl<-numeric(n.obs)

for(i in 1:n.obs){
  time.i<-Ti[i]
  #On calcule SY(Ti)
  time<-H0.nl$time
  cumhaz<-H0.nl$cumhaz
  if (time.i > max(time)){  H0.Ti <- max(cumhaz) 
  }else H0.Ti<-min(cumhaz[time>=time.i])
  
  SY.Ti<-exp(-exp(log.risk[i])*H0.Ti)
  
  #On calcule SC(Ti)
  time<-KM.censor$time
  surv<-KM.censor$surv
  if (time.i > max(time)){ SC.Ti <- min(surv) 
  }else SC.Ti<-max(surv[time>=time.i])
  
  Qi.nl[i]<-SY.Ti*SC.Ti
}

#Représentation graphique 
plot((1:length(Qi.lin))/length(Qi.lin),sort(Qi.lin),col="red",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
points((1:length(Qi.nl))/length(Qi.nl),sort(Qi.nl),col="blue",ylim=c(0,1),type='l',main="Q-Q uniform plot/calibration")
abline(a=0,b=1,lty=2)

